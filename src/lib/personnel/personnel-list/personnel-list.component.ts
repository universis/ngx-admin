import {
  Component,
  AfterViewInit,
  Input,
  ViewChild,
  OnDestroy
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import {
  AdvancedTableComponent,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import { AdvancedTableSearchComponent } from '@universis/ngx-tables';
import { ActivatedTableService } from '@universis/ngx-tables';

@Component({
  selector: 'lib-personnel-list',
  templateUrl: './personnel-list.component.html',
  styles: []
})
export class PersonnelListComponent implements AfterViewInit, OnDestroy {
  private dataSubscription: Subscription;
  private fragmentSubscription: Subscription;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  public recordsTotal: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService
  ) {}

  ngAfterViewInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe((data) => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        this.advancedSearch.getQuery().then((res) => {
          this.table.destroy();
          this.table.query = res;
          this.advancedSearch.text = '';
          this.table.fetch(false);
        });
      }
    });
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(
      (fragment) => {
        if (fragment && fragment === 'reload') {
          this.table.fetch();
        }
      }
    );
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }
}
