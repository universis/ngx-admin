import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';
import {UserRootComponent} from './users/user-root/user-root.component';
import {UsersListComponent} from './users/users-list/users-list.component';
import {UserEditComponent} from './users/user-root/user-edit/user-edit.component';
import {UserGroupsComponent} from './users/user-root/user-groups/user-groups.component';
import {UserDepartmentsComponent} from './users/user-root/user-departments/user-departments.component';
import {GroupsComponent} from './groups/groups.component';
import {GroupsListComponent} from './groups/groups-list/groups-list.component';
import {GroupRootComponent} from './groups/group-root/group-root.component';
import {GroupEditComponent} from './groups/group-root/group-edit/group-edit.component';
import {GroupUsersComponent} from './groups/group-root/group-users/group-users.component';
import {UsersListConfigurationResolver, UsersListSearchResolver} from './users/users-list/users-list-config.resolver';
import {AdvancedFormItemResolver, AdvancedFormModalComponent, AdvancedFormModalData} from '@universis/forms';
import { PrivilegesComponent } from './privileges/privileges.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { GroupPrivilegesComponent } from './groups/group-privileges/group-privileges.component';
import { UserModificationHistoryComponent } from './users/user-modification-history/user-modification-history.component';
import { RemoveUserComponent } from './users/remove-user/remove-user.component';
import { PersonnelComponent } from './personnel/personnel.component';
import { PersonnelListComponent } from './personnel/personnel-list/personnel-list.component';
import { PersonnelListConfigurationResolver, PersonnelListSearchResolver } from './personnel/personnel-list/personnel-list-config.resolver';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'users',
    pathMatch: 'full',
  },
  {
    path: 'users',
    data: {
      title: 'Users'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/index',
      },
      {
        path: 'list',
        pathMatch: 'full',
        redirectTo: 'list/list'
      },
      {
        path: 'list/index',
        data: {
          title: 'Users list'
        },
        component: UsersListComponent,
        resolve: {
          tableConfiguration: UsersListConfigurationResolver,
          searchConfiguration: UsersListSearchResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData> {
              model: 'Users',
              closeOnSubmit: true,
              action: 'new',
            }
          },
        ]
      },
      {
        path: ':id',
        component: UserRootComponent,
        data: {
          title: 'Admin.Users.User'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'edit'
          },
          {
            path: 'edit',
            component: UserEditComponent,
            data: {
              title: 'Admin.Users.Edit'
            },
            children: [
              {
                path: 'remove',
                outlet: 'modal',
                component: RemoveUserComponent
              }
            ]
          },
          {
            path: 'groups',
            component: UserGroupsComponent,
            data: {
              title: 'Admin.Users.Groups'
            }
          },
          {
            path: 'departments',
            component: UserDepartmentsComponent,
            data: {
              title: 'Admin.Users.Departments'
            }
          },
          {
            path: 'history',
            component: UserModificationHistoryComponent
          }
        ]
      }
    ]
  },
  {
    path: 'groups',
    component: GroupsComponent,
    data: {
      title: 'Groups'
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: GroupsListComponent,
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData> {
              model: 'Groups',
              closeOnSubmit: true,
              action: 'new',
            }
          },
        ]
      },
      {
        path: ':id',
        component: GroupRootComponent,
        data: {
          title: 'Admin.Groups.Group'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'edit'
          },
          {
            path: 'edit',
            component: GroupEditComponent,
            data: {
              title: 'Admin.Groups.Edit'
            }
          },
          {
            path: 'users',
            component: GroupUsersComponent,
            data: {
              title: 'Admin.Groups.Groups'
            }
          },
          {
            path: 'privileges',
            component: GroupPrivilegesComponent,
            data: {
              title: 'Admin.Groups.Privileges'
            }
          }
        ]
      }
    ]
  },
  {
    path: 'privileges',
    component: PrivilegesComponent,
    data: {
      title: 'Permission management'
    }
  },
  {
    path: 'privileges/:privilege/manage',
    component: PermissionsComponent
  },
  {
    path: 'personnel',
    component: PersonnelComponent,
    data: {
      title: 'Personnel'
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: PersonnelListComponent,
        children: [
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'Personnels',
              action: 'edit',
              closeOnSubmit: true,
              serviceQueryParams: {
              }
            },
            resolve: {
              data: AdvancedFormItemResolver
            }
          },
        ],
        resolve: {
          tableConfiguration: PersonnelListConfigurationResolver,
          searchConfiguration: PersonnelListSearchResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
