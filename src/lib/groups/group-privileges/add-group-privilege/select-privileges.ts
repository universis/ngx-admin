export const SELECT_PRIVILEGES_CONFIG = {
  title: 'Privileges',
  model: 'Privileges',
  searchExpression:
    'indexof(alternateName, \'${text}\') ge 0 or indexof(name, \'${text}\') ge 0 or indexof(tags/name, \'${text}\') ge 0',
  selectable: true,
  multipleSelect: true,
  columns: [
    {
      name: 'id',
      property: 'id',
      hidden: true,
    },
    {
      name: 'name',
      property: 'name',
      title: 'Admin.PrivilegeName',
    },
    {
      name: 'tags',
      property: 'tags',
      virtual: true,
      sortable: false,
      title: 'Admin.PrivilegeCategory',
      formatters: [
        {
          formatter: 'TemplateFormatter',
          formatString:
            '${tags.length ? tags.map(x => x.name).join(\', \') : \'-\'}',
        },
      ],
    },
    {
      name: 'alternateName',
      property: 'alternateName',
      title: 'Admin.PrivilegeAlternateName',
    },
    {
      name: 'description',
      property: 'description',
      title: 'Admin.PrivilegeDescription',
    },
    {
      name: 'mask',
      virtual: true,
      property: 'mask',
      formatter: 'PermissionMaskFormatter',
      title: 'Admin.PrivilegeTypes',
    },
    {
      name: 'mask',
      property: 'mask',
      hidden: true,
    },
    {
      name: 'appliesTo',
      property: 'appliesTo',
      hidden: true,
    },
  ],
  defaults: {
    expand: 'tags',
  },
  criteria: [
    {
      name: 'tag',
      filter: '(tags/id eq ${value})',
      type: 'text',
    },
  ],
  searches: [],
  paths: [],
};
