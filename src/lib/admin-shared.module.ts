import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { environment } from './environments/environment';
import { ADMIN_LOCALES } from './i18n';
import { AdminService } from './admin.service';
import { PermissionsComponent } from './permissions/permissions.component';
import { COLUMN_FORMATTERS, FORMATTERS, TablesModule} from '@universis/ngx-tables';
import { AdvancedFormsModule } from '@universis/forms';
import {UsersListConfigurationResolver, UsersListSearchResolver} from './users/users-list/users-list-config.resolver';
import { DefaultPrivilegesComponent } from './default-privileges/default-privileges.component';
import { CheckBoxFormatter } from './permissions/checkbox-formatter';
import { PermissionMaskFormatter } from './groups/group-privileges/permission-mask-formatter';
import { AddGroupPrivilegeComponent } from './groups/group-privileges/add-group-privilege/add-group-privilege.component';
import { FormsModule } from '@angular/forms';
import { DefaultGroupPrivilegesComponent } from './groups/group-privileges/default-group-privileges/default-group-privileges.component';

export function ExtendedColumnFormattersFactory() {
  return {
    ...FORMATTERS,
    CheckBoxFormatter: new CheckBoxFormatter(),
    PermissionMaskFormatter: new PermissionMaskFormatter()
  };
}
import { RequiredServerPrivilegesDirective } from './directives/requiredServerPrivileges';
import { PersonnelListConfigurationResolver, PersonnelListSearchResolver } from './personnel/personnel-list/personnel-list-config.resolver';
import { IfEntityDirective } from './directives/if-entity.directive';

@NgModule({
    imports: [
        TranslateModule,
        CommonModule,
        TablesModule,
        AdvancedFormsModule,
        FormsModule
    ],
    declarations: [
      PermissionsComponent,
      DefaultPrivilegesComponent,
      AddGroupPrivilegeComponent,
      DefaultGroupPrivilegesComponent,
      RequiredServerPrivilegesDirective,
      IfEntityDirective
    ],
    exports: [
      PermissionsComponent,
      DefaultPrivilegesComponent,
      AddGroupPrivilegeComponent,
      DefaultGroupPrivilegesComponent,
      RequiredServerPrivilegesDirective,
      IfEntityDirective
    ],
    entryComponents: [
      AddGroupPrivilegeComponent,
      DefaultGroupPrivilegesComponent
    ],
    providers: [
      AdminService,
      UsersListConfigurationResolver,
      UsersListSearchResolver,
      PersonnelListConfigurationResolver,
      PersonnelListSearchResolver,
      {
        provide: COLUMN_FORMATTERS,
        useFactory: ExtendedColumnFormattersFactory,
      }
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
  })
  export class AdminSharedModule {
    constructor( @Optional() @SkipSelf() parentModule: AdminSharedModule, private translateService: TranslateService) {
      this.ngOnInit();
    }

    static forRoot(): ModuleWithProviders<AdminSharedModule> {
      return {
        ngModule: AdminSharedModule,
        providers: [
          AdminService
        ]
      };
    }
    // tslint:disable-next-line:use-life-cycle-interface use-lifecycle-interface
    ngOnInit() {
      environment.languages.forEach( (language) => {
        if (ADMIN_LOCALES.hasOwnProperty(language)) {
          this.translateService.setTranslation(language, ADMIN_LOCALES[language], true);
        }
      });
    }

  }
