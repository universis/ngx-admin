import { Component, OnDestroy, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {AppEventService} from '@universis/common';
import { Subscription } from 'rxjs';

@Component({
  selector: 'lib-group-root',
  templateUrl: './group-root.component.html',
  styles: []
})
export class GroupRootComponent implements OnInit, OnDestroy {
  public group: any;
  public isCreate = false;
  public config: any;
  public edit: any;
  private changeSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _appEvent: AppEventService) {
  }

  async ngOnInit() {
    if (this._activatedRoute.snapshot.params.id) {
      this.loadData();
    } else {
      this.isCreate = true;
    }

    this.changeSubscription = this._appEvent.change.subscribe(event => {
      if (event && event.model === 'Groups' && this._activatedRoute.snapshot.params.id) {
        this.loadData();
      }
    });
  }

  loadData() {
    this._context.model('groups')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('scopes', 'tags')
      .getItem()
      .then(group => {
        this.group = group;
        this._appEvent.change.next({
          model: 'Group',
          target: this.group
        });
      });
  }

  ngOnDestroy() {
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

}
